package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Comment(
    var id:Int,
    var comment:String,
    var creationDate:String
)