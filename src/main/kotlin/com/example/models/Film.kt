package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Film(
    val id:Int,
    var title:String,
    var year:String,
    var genere:String,
    var director:String,
    val commentsStorage:MutableList<Comment>
)

val filmsStorage = mutableListOf<Film>()