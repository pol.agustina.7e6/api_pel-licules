package com.example.routes

import com.example.models.Comment
import com.example.models.Film
import com.example.models.filmsStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.filmRouting(){
    route("/films"){
        get {
            if (filmsStorage.isNotEmpty()) {
                call.respond(filmsStorage)
            } else {
                call.respondText(
                    "No films found",
                    status = HttpStatusCode.OK
                )
            }
        }
        get("{id?}") {
            if(call.parameters["id"].isNullOrBlank()) {
                return@get call.respondText(
                    "Missing id",
                    status = HttpStatusCode.BadRequest
                )
            }
            val id = call.parameters["id"]?.toInt()
            for (film in filmsStorage) {
                if(film.id == id) return@get call.respond(film)
            }
            call.respondText(
                "No film with id $id",
                status = HttpStatusCode.NotFound
            )
        }

        post {
            val film = call.receive<Film>()
            filmsStorage.add(film)
            call.respondText(
                "Film stored correctly",
                status = HttpStatusCode.Created)
        }

        put("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@put call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]?.toInt()
            val filmUpdate = call.receive<Film>()
            for (film  in filmsStorage) {
                if (film.id == id) {
                    film.title = filmUpdate.title
                    film.year = filmUpdate.year
                    film.genere = filmUpdate.genere
                    film.director = filmUpdate.director
                    return@put call.respondText(
                        "Film with id $id has been updated",
                        status = HttpStatusCode.Accepted
                    )
                }
            }
            call.respondText(
                "Film with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }
        delete("{id?}") {
            if(call.parameters["id"].isNullOrBlank()) {
                return@delete call.respondText(
                    "Missing id",
                    status = HttpStatusCode.BadRequest
                )
            }
            val id = call.parameters["id"]?.toInt()
            for (film in filmsStorage) {
                if(film.id == id) {
                    filmsStorage.remove(film)
                    return@delete call.respondText(
                        "Film removed correctly",
                        status = HttpStatusCode.Accepted
                    )
                }
            }
            call.respondText(
                "No film with id $id",
                status = HttpStatusCode.NotFound
            )
        }

        route("{id?}/comments"){
            get {
                val id = call.parameters["id"]?.toInt()
                for (film in filmsStorage) {
                    if(film.id == id) {
                        if (film.commentsStorage.isNotEmpty()) {
                            call.respond(film.commentsStorage)
                        } else {
                            call.respondText(
                                "No comments found",
                                status = HttpStatusCode.OK
                            )
                        }
                    }
                }
            }

            post{
                val comment = call.receive<Comment>()
                val id = call.parameters["id"]?.toInt()
                for (film in filmsStorage) {
                    if(film.id == id) {
                        film.commentsStorage.add(comment)
                        call.respondText(
                            "Comment stored correctly",
                            status = HttpStatusCode.Created
                        )
                    }
                }
            }
        }
    }
}